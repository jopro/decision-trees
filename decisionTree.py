from math import log

class DecisionTree:
    def __init__(self, samples, numTraits, toUse=None):
        self.children = []
        self.samples = samples
        self.numTraits = numTraits
        self.trait = None
        self.case = None
        if toUse == None:
            toUse = range(self.numTraits-1)#last item is the case
        self.traitsToUse = toUse

    def __str__(self, depth = -1):
        o = ""
        depth += 1
        if len(self.children) == 0:
            case = self.allSameCase()
            if case != None:
                o += "\t"*depth+str(case)+"\n"
            else:
                for sample in self.samples:
                    sample = ", ".join(map(str, sample))
                    o += "\t"*depth+sample+"\n"

        for child in self.children:
            o += "\t"*depth+str(child.samples[0][self.trait])
            o += child.__str__(depth)
        return o

    def getEntropy(self, samples):
        caseSizes = {}
        for sample in samples:
            case = sample[-1]
            caseSizes[case] = caseSizes.get(case, 0)+1
        
        e = 0#calculate the entropy
        for case, size in caseSizes.items():
            p = size/float(len(samples))
            e -= p*log(p, 2)
        return e

    def classify(self):
        bestE = 1
        bestGroups = {}
        for index in self.traitsToUse:#for decision type
            #find the number of samples of each type
            traitGroups = {}
            for dna in self.samples:
                trait = dna[index]
                traitGroups[trait] = traitGroups.get(trait, [])
                traitGroups[trait].append(dna)
            
            totalE = 0
            totalSize = 0
            for trait, samples in traitGroups.items():
                size = len(samples)
                totalE += size*self.getEntropy(samples)
                totalSize += size
           
            e = 1
            if totalSize > 0:
                e = totalE/float(totalSize)

            if e < bestE:
                bestE = e
                self.trait = index
                bestGroups = traitGroups

        if self.trait != None:
            self.createChildren(bestGroups)

    def createChildren(self, traitGroups):
        subTraits = list(self.traitsToUse)
        subTraits.remove(self.trait)
        #create the children based on the data
        for choice in traitGroups.keys():
            newChild = DecisionTree(traitGroups[choice], self.numTraits, subTraits)
            if not newChild.allSameCase():
                newChild.classify()
            self.children.append(newChild)

    def allSameCase(self):
        if len(self.samples) == 0: return None
        case = self.samples[0][-1]#get a case
        for sample in self.samples[1:]:
            if sample[-1] != case: return None
        return case

    def prune(self):
        #evalute Laplace error
        n = 0
        for child in self.children:
            childN = len(child.samples)
            if childN > n:
                n = childN
        k = len(self.children)
        N = len(self.samples)
        self.error = 1.0 - (n+1.0)/(N+k)
        
        if n == 0:
            return True

        #call prune on children, if they return true then prune on this one
        allLeaves = k != 0
        totalError = 0
        for child in self.children:
            if not child.prune():
                allLeaves = False
            else:
                totalError += child.error

        if allLeaves:
            if self.error < totalError/float(k):#if the decision introduces more error
                self.children = []#remove it
        return len(self.children) == 0

def main():

    samples = [
            ["short",   "blond",    "blue", "+"],
            ["tall",    "red",      "blue", "+"],
            ["tall",    "red",      "green", "-"],#added ???
            ["tall",    "blond",    "blue", "+"],
            ["tall",    "blond",    "brown", "-"],
            ["short",   "dark",     "blue", "-"],
            ["tall",    "dark",     "blue", "-"],
            ["tall",    "dark",     "brown", "-"],
            ["short",   "blond",    "brown", "-"]
            ]
    numTraits = 4
    tree = DecisionTree(samples, numTraits)
    tree.classify()
    tree.prune()
    print str(tree)

if __name__ == "__main__":
    main()
